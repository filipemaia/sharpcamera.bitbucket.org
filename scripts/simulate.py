#!/usr/bin/env python

import numpy as np
import h5py

scan_side = 20 # Scan side in frames
nframes = scan_side**2
[X,Y] = np.meshgrid(range(0,scan_side),range(0,scan_side))

start_x = 0;
start_y = 0
end_x = scan_side
end_y = scan_side

# Keep in mind that the translation refers to the sample moving, not the illumination!
translation = np.column_stack((X[start_y:end_y,start_x:end_x].flatten(),Y[start_y:end_y,start_x:end_x].flatten(),np.zeros(Y[start_y:end_y,start_x:end_x].size)))


step_size = 40 * 1e-9 # Scan step size in m
pixel_step = 4 # Step size in pixels
det_side = 32 # Detector side in pixels

obj_side = det_side+(scan_side-1)*pixel_step

[X,Y] = np.meshgrid(np.arange(-np.pi,np.pi,2.0*np.pi/obj_side),np.arange(-np.pi,np.pi,2.0*np.pi/obj_side))
object = np.sin(X)*np.sin(X*Y) # Our test object

# CXI and python have opposite x and y axis so we don't have to reverse 
# the translation to select the areas of the object we want to illuminate
pixel_translation = translation * pixel_step


# Generate the probe
[X,Y] = np.meshgrid(np.arange(-3,3,6./det_side),np.arange(-3,3,6./det_side))
R = np.sqrt(X**2+Y**2)
# Use a relatively tight constraint
probe_mask = R < 0.3
probe = np.complex128(probe_mask)
probe *= np.random.random(probe.shape)
probe = np.fft.fftshift(np.fft.fft2(probe))

frames = np.empty((nframes,det_side,det_side))
rframes = np.empty((nframes,det_side,det_side))
for i in range(0,nframes):
    frames[i] = object[pixel_translation[i,1]:pixel_translation[i,1]+det_side,pixel_translation[i,0]:pixel_translation[i,0]+det_side]
    # Transform to intensities, no oversampling
    # Implicity assumes illumination is square and uniform
    rframes[i] = frames[i]
    frames[i] = np.abs(np.fft.fftshift(np.fft.fft2(probe*frames[i])))**2

data_white = np.ones((10,det_side,det_side))
for i in range(0,10):
    data_white[i] = np.abs(np.fft.fftshift(np.fft.fft2(probe)))**2

#probe = np.complex128(np.random.random((det_side,det_side)))
#probe = np.complex128(np.ones((det_side,det_side)))


real_translation = translation * step_size
energy = 1000*1.60217657e-19 # 1keV in J
wavelength = 1.98644e-25/energy
pixel_size = 100e-6 # 100 um pixels

# Calculate distance to detector such that the step sizes in meters and pixels agree
max_q = 1/(2*step_size/pixel_step)
distance = (det_side/2 * pixel_size)/(wavelength*max_q)
corner_pos = [det_side/2*pixel_size,det_side/2*pixel_size,distance]

# Write out input file
f = h5py.File('simulated.cxi', "w")
f.create_dataset("cxi_version",data=140)
entry_1 = f.create_group("entry_1")
instrument_1 = entry_1.create_group("instrument_1")
source_1 = instrument_1.create_group("source_1")
source_1.create_dataset("energy", data=energy) # in J
detector_1 = instrument_1.create_group("detector_1")
data = detector_1.create_dataset("data",data=frames)
data.attrs['axes'] = "translation:y:x"
data = detector_1.create_dataset("probe_mask",data=probe_mask)
detector_1.create_dataset("distance", data=distance) # in meters
detector_1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')
detector_1.create_dataset("x_pixel_size", data=pixel_size) # in meters
detector_1.create_dataset("y_pixel_size", data=pixel_size) # in meters
detector_1.create_dataset("corner_position", data=corner_pos) # in meters
sample_1 = entry_1.create_group("sample_1")
geometry_1 = sample_1.create_group("geometry_1")
geometry_1.create_dataset("translation", data=real_translation) # in meters
data_1 = entry_1.create_group("data_1")
data_1["data"] = h5py.SoftLink('/entry_1/instrument_1/detector_1/data')
data_1["translation"] = h5py.SoftLink('/entry_1/sample_1/geometry_1/translation')
f.close()

# Try to reconstruct with
# $ sharp -R  -M -r 10  -i 1000  simulated.cxi
